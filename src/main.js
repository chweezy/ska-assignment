
import Vue from 'vue';

import vuetify from './plugins/vuetify';
//import store from './store/store.js';
import App from './App';
import router from './routes/index'
import 'vuetify/dist/vuetify.min.css';


Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('main-nav', require('./components/client/MainNav.vue').default);
Vue.component('footer-nav', require('./components/client/FooterNav.vue').default);

const app = new Vue({
    el: '#app',
    router,
    //store,
    vuetify,
    render: h=>h(App)
});

export default app;
