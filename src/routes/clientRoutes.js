const WebHome = () =>import('../pages/client/WebHome.vue')
const AboutUs = ()=>import('../pages/client/AboutUs.vue');
const ClientNews = ()=>import('../pages/client/ClientNews.vue');
const BusinessTable = () => import('../pages/client/products/BusinessTable.vue');
const ServicesTable = () => import('../pages/client/products/ServicesTable.vue');
const GetInsuarance = () => import ('../pages/client/products/GetInsuarance')
//const Register = ()=>import('../pages/frontend/auth/Register');
//const ForgotPassword = ()=>import('../pages/frontend/auth/ForgotPassword');

export default[
    {
        name:'WebHome',
        path:'/',
        component:WebHome
    }, 
    {
        name:'AboutUs',
        path:'/about-us',
        component:AboutUs
    },
    {
        name:'News',
        path:'/news',
        component:ClientNews
    },
    {
        name:'BusinessTable',
        path:'/business',
        component:BusinessTable
    },
    {
        name:'ServicesTable',
        path:'/services',
        component:ServicesTable
    },
    {
        name:'GetInsuarance',
        path:'/apply',
        component:GetInsuarance
    },
    

]