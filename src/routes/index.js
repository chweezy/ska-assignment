import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
//import NProgress from "nprogress"
//import "nprogress/nprogress.css"
import clientLayout from '../layouts/ClientLayout';
import clientRoutes from '../routes/clientRoutes';


const router = new VueRouter({
    routes:[
        {
            name:'home',
            component: clientLayout,
            path:'/',
            children:[
                ...clientRoutes
            ]
        }
    ],
});



export default router;